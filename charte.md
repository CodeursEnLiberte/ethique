# 1. Introduction

Nous appliquons notre charte d’éthique aux projet auxquels nous participons.

Il s’agit d’idéaux, d’objectifs à atteindre, pas d’une liste à cocher. Nous sommes faillibles, et ce texte est imparfait.
Nous sommes conscient·e·s que cette charte ne répond pas à la complexité de toutes les questions éthiques, et nous nous engageons à questionner nos pratiques, et à adapter cette charte à mesure. 

# 2. Principes
* Avoir une influence sociale positive ;
* Respecter les utilisateurs ;
* Préférer la qualité à la précipitation ;
* Être transparent ;
* Contribuer à la création de communs.
En tout état de cause, c’est le bien commun qui prime.

# 3 Code
## 3.1 S’engager à  un travail de qualité
### 3.1.1. Concevoir et mettre en place des logiciels fiables
* Privilégier les projets pérennes, non-jetables.

### 3.1.2. S’assurer de la sécurité des logiciels déployés
* Faciliter la maintenance et la mise à jour sur le long terme.

### 3.1.3. Être transparent.
* Donner des estimations de bonne foi, en temps comme en prix.
* Admettre ses lacunes et donner un avis basé sur des connaissances suffisantes.
* Ne pas masquer des difficultés.
* Ne pas se rendre essentiel : faire en sorte que le projet puisse être repris par quelqu’un d’autre.

### 3.1.4. Favoriser l’open source
* Respecter les licences des logiciels open-source utilisés.
  * Le cas échéant, contribuer en retour.
* Privilégier des solutions open-source pour les outils utilisés.
* Open-sourcer ses propres réalisations par défaut.

## 3.2 Agir pour le bien commun
### 3.2.1. Ne pas mettre en danger les utilisateurs·trices
* De façon volontaire: ne pas travailler pour l’industrie de l’armement et ne pas développer de service assimilable à des armes.
* De façon accidentelle (*cas des véhicules autonomes.*).
* Ne pas non plus mettre en danger les *non*-utilisateurs·trices.

### 3.2.2. Ne pas discriminer les utilisateurs·trices
* S’assurer de l’usabilité par des utilisateurs·trices divers.
* Évaluer les conséquences discriminantes lors de l’utilisation d’algorithmes d’apprentissage-machine (“machine learning”).

### 3.2.3. Ne pas privilégier les utilisateurs·trices aux non-utilisateurs·trices
* Permettre l’export complet de données personnelles, la désinscription et le retrait total.

### 3.2.4. Être honnête vis-à-vis des utilisateurs·trices ; ne pas les manipuler
* Expliciter le modèle commercial derrière un service et s’assurer que les conditions d’utilisation soient compréhensibles.
* Ne pas prendre d’initiative au nom de l’utilisat·eur·rices·s.

### 3.2.5. Protéger la vie privée
* Ne pas stocker plus d’informations que nécessaire.
* Ne pas publier d’informations personnelles.
* Ne pas revendre de données personnelles.
* Le droit à la vie privée s’applique à des personnes privées : les actions de personnes publiques, l’utilisation d’argent public ou les atteintes au bien commun — comme des dégradations écologiques — ne sont pas concernées.

### 3.2.6. Contribuer à la création de communs
sous la forme de code (open source), de données (open data) et de savoir.

* Ne pas travailler pour des chasseurs de brevets (“patent trolls”).
* Ne pas utiliser de financements publics pour des développements privés.
* Encourager des alternatives au capitalisme, notamment des mouvements de coopératives.

# 4. Mise en œuvre
Ce texte n’a pas force de loi ; nous l’appliquons à nous-mêmes.
Nous nous engageons à évaluer nos projets vis-à-vis de ce code et refuser des projets en désaccord avec celui-ci. Nous sommes conscients des limitations ; les cas ambigus serviront à l’améliorer.

Enfin, nous souhaitons déclencher des discussions dans la communauté, et encourager l’usage d’une charte d’éthique au sein de la profession.

