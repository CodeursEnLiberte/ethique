# Liens et ressources

* Le Code of Ethics Professional conduct de l’[Association for Computing Machinery](https://www.acm.org) (s’applique aux membres de l’ACM)
 * [Révision 2018](https://www.acm.org/code-of-ethics)
 * Ainsi que le [Software Engineering Code](https://ethics.acm.org/code-of-ethics/software-engineering-code/), établi avec l’IEEE et adoptable par tous dans la profession. 
 * [Version de 1992, traduite en français](http://ethics.acm.org/wp-content/uploads/2016/07/seeri.french.code_.one_.column.pdf?32ddaa)
* Le [Code de déontologie des ingénieurs](http://legisquebec.gouv.qc.ca/fr/ShowDoc/cr/I-9,%20r.%206/), texte législatif au Québec.
* Un projet similaire (avorté?) d’[Ordre des développeurs](https://ordre-des-developpeurs.gitbooks.io/constitution/content/code.html)
* Un autre projet similaire, [the Turing Oath](https://github.com/mixinmax/Turing-Oath)
* http://www.libre-entreprise.org/charte

# Cas et études de cas

* Le Software Ethics Casebook par Ariel Elkin https://software-ethics-casebook.github.io
* [Des notes de Pierre](https://github.com/kemenaran/developers-oath)
* [Un thread de @taniki sur Cambridge Analytica](https://mobile.twitter.com/taniki/status/985102726374838272)
* [Wired: To work for society, data scientists need a hippocratic oath with teeth](http://www.wired.co.uk/article/data-ai-ethics-hippocratic-oath-cathy-o-neil-weapons-of-math-destruction)

# Articles

* [Graham Lee à propos du code de l’ACM](https://www.sicpers.info/2017/11/my-platform-is-no-platform/)
* Des notes sur Clean Code: [The programmer’s Oath](http://blog.cleancoder.com/uncle-bob/2015/11/18/TheProgrammersOath.html) et [We Programmers](http://blog.cleancoder.com/uncle-bob/2018/03/29/WeProgrammers.html).
* [Matthew Bischoff: Activist Engineering](http://matthewbischoff.com/activist-engineering)
* [Code de déontologie des coachs professionels](http://www.sfcoach.org/deontologie)
* [Code de déontologie du designer professionel](http://www.alliance-francaise-des-designers.org/afd-code-de-deontologie-du-designer-professionnel.html) de l’Alliance Française des Designers
* [France Culture: Éthique numérique, des datas sous serment](https://www.franceculture.fr/emissions/la-methode-scientifique/ethique-numerique-des-datas-sous-serment)
* [Bill Sourour: The code I’m still ashamed of](https://medium.freecodecamp.org/the-code-im-still-ashamed-of-e4c021dff55e)

# Documentations sur l’éthique
* Sur wikipedia, [Engineering Ethics](https://en.wikipedia.org/wiki/Engineering_ethics) et [Computer Ethics](https://en.wikipedia.org/wiki/Computer_ethics), ainsi que le [Serment d’Hippocrate](https://fr.wikipedia.org/wiki/Serment_d%27Hippocrate)
* http://gpp.oiq.qc.ca/distinction_entre_ethique_deontologie.htm

