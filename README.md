« Je n’ai fait qu’appliquer les spécifications. »

« Moi, je m’occupe d’écrire le logiciel. Ce qu’en font les utilisateurs, ce n’est pas mon problème. »

« Je sais que c’est critiquable, mais c’est bien payé. »

« Perso, je n’utilise pas le produit sur lequel je travaille. D’ailleurs je dis à mes amis de ne pas l’utiliser. »

Peut-être avez vous déjà entendu l’un de vos collègues dire quelque chose comme ça. Ça nous arrive parfois aussi de le penser nous-mêmes. Nous, les développeur·se·s et les professionnels de l’informatique, avons parfois du mal à mettre en rapport nos pratiques professionelles et notre morale individuelle.

Pourtant, les logiciels contrôlent une part de plus en plus importante de notre société, et nous avons un énorme pouvoir entre nos doigts. Nous n’en avons pas toujours conscience ; nous en assumons rarement la responsabilité.

On a vu les conséquences parfois néfastes des logiciels sur la société, dans pas mal de domaines :

* Vie privée : Facebook/fakenews/Cambridge Analytica/exodus privacy/GDPR.
* Mise en danger : véhicules autonomes, Tesla ou Uber, mais plus largement aéronautique ou transit.
* Pollution : Volkswagen « dieselgate » (et tous les constructeurs automobiles).
* Contournement de la réglementation : Uber Grayball & Volkswagen.
* Fabrication d’armes : réelles (guidage de missile, par exemple), ou numériques (attaques informatiques, intrusions).
* Filtrage d’internet, surveillance de masse.

Si vous en doutez encore, vous pouvez lire les 494 commentaires laissés sur Hacker News quand quelqu'un a demandé « [Quelle est la chose la moins éthique que vous ayez fait en tant que développeur?](https://news.ycombinator.com/item?id=17692005) »

À Codeurs en Liberté, il nous semble qu’une charte d’éthique est devenue nécessaire pour encadrer notre action. Nous comptons commencer à l’appliquer nous-mêmes, et invitons la communauté à faire la même démarche. Nous sommes d’ailleurs loin d’être les premiers : en particulier, l’[ACM](https://ethics.acm.org) et le mouvement ces derniers mois [#techwontbuildit](https://twitter.com/hashtag/TechWontBuildIt).

Nous ne voulons pas seulement « éviter de nuire »: nous croyons encore que l’informatique à la responsabilité d’améliorer la société. Et dans le marché du travail actuel, nous avons le luxe de pouvoir choisir nos clients ; si nous ne refusons pas les projets nocifs, qui le fera ?
